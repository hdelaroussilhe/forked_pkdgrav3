SUBDIRS=openpa
ACLOCAL_AMFLAGS  = -I m4

SUFFIXES = .cu

.cu.o:
	$(NVCC) -I. -DHAVE_CONFIG_H -lineinfo -o $@ -c $< $(NVML_INCLUDE) $(NVCCFLAGS) -Imdl2/mpi -Imdl2 -Iopenpa/src

EXTRA_DIST = server.py examples/cosmology.par examples/euclid_z0_transfer_combined.dat
EXTRA_PROGRAMS = 
TARGETS = 

if HAVE_MPI
EXTRA_PROGRAMS	+= pkdgrav3_mpi
TARGETS	+= mpi

mpi	: pkdgrav3_mpi

#	$(MAKE) CC=@MPICC@ CXX=@MPICXX@ FC=@MPIFC@ $(AM_MAKEFLAGS) pkdgrav3_mpi
endif

EXTRA_PROGRAMS += tostd tdiff hpb2fits lcp2tip

all-local: $(TARGETS)

clean-local:
	rm -f $(EXTRA_PROGRAMS)

SOURCES = main.c tree.c analysis.c bt.c
noinst_HEADERS = bt.h parameters.h grav.h walk.h mdl2/cycle.h tipsydefs.h basetype.h vqsort.h
SOURCES        += cosmo.c master.c param.c tinypy.c pkdtinypy.c pst.c smooth.c ewald.cxx opening.cxx pp.cxx pc.cxx
noinst_HEADERS += cosmo.h master.h param.h tinypy.h pst.h smooth.h ewald.h pp.h pc.h meval.h qeval.h
SOURCES        += moments.c outtype.c pkd.c smoothfcn.c rbtree.c fio.c listcomp.c illinois.c iomodule.c output.c
noinst_HEADERS += moments.h intype.h outtype.h pkd.h smoothfcn.h rbtree.h fio.h listcomp.h illinois.h iomodule.h output.h

SOURCES += healpix.c
noinst_HEADERS += healpix.h

SOURCES += hop.c fof.c group.c groupstats.c
noinst_HEADERS += hop.h fof.h group.h groupstats.h

SOURCES += $(MDL)/mdlbase.c
noinst_HEADERS += $(MDL)/mdlbase.h

SOURCES += grav2.c walk2.c
SOURCES += lst.c ilp.c ilc.c cl.c
noinst_HEADERS += lst.h ilp.h ilc.h cl.h
if USE_SIMD
if USE_SIMD_FMM
SOURCES        += vmoments.cxx
noinst_HEADERS += vmoments.h
endif
if USE_SIMD_LC
SOURCES        += lightcone.cxx
noinst_HEADERS += lightcone.h
endif
AM_CFLAGS = @SIMD_CFLAGS@
AM_CXXFLAGS = @SIMD_CFLAGS@
noinst_HEADERS += simd.h vmath.h
endif
MDL=@MDL_PATH@
if USE_PNG
SOURCES += png.c
noinst_HEADERS += png.h
endif
if USE_PYTHON
SOURCES += pkdpython.c
noinst_HEADERS += pkdpython.h
endif
if USE_CUDA
SOURCES += cudautil.cu cudapppc.cu cudaewald.cu
noinst_HEADERS += cudautil.h
endif
if USE_CL
SOURCES += clutil.cxx clewald.cxx clpp.cxx
noinst_HEADERS += clutil.h
endif

if HAVE_MPI
pkdgrav3_mpi_LDADD = -lpthread
pkdgrav3_mpi_SOURCES = $(SOURCES) $(MDL)/mpi/mdl.c openpa/src/opa_primitives.c openpa/src/opa_queue.c
noinst_HEADERS += $(MDL)/mpi/mdl.h
pkdgrav3_mpi_CFLAGS = -I$(MDL)/mpi/ -I$(MDL) -Iopenpa/src $(AM_CFLAGS)
pkdgrav3_mpi_CXXFLAGS = -I$(MDL)/mpi/ -I$(MDL) -Iopenpa/src $(AM_CXXFLAGS)
if FFTW
pkdgrav3_mpi_SOURCES += ic.cxx RngStream.c
noinst_HEADERS += ic.h RngStream.h
endif
pkdgrav3_mpi_CFLAGS += $(GSL_CFLAGS)
pkdgrav3_mpi_CXXFLAGS += $(GSL_CFLAGS)
pkdgrav3_mpi_LDADD += $(GSL_LIBS)
if USE_LIBAIO
pkdgrav3_mpi_LDADD += -laio
if !HAVE_DARWIN
pkdgrav3_mpi_LDADD += -lrt
endif
else
if USE_AIO
if !HAVE_DARWIN
pkdgrav3_mpi_LDADD += -lrt
endif
endif
endif
if HAVE_MEMKIND
pkdgrav3_mpi_LDADD += -lmemkind
endif
if USE_EFENCE
if HAVE_EFENCE_PATH
pkdgrav3_mpi_LDADD += -L @EFENCE_PATH@
endif
pkdgrav3_mpi_LDADD += -lefence
endif
if USE_ITT
if HAVE_ITT_PATH
pkdgrav3_mpi_LDADD += -L @ITT_PATH@/lib64
pkdgrav3_mpi_CFLAGS += -I @ITT_PATH@/include
endif
pkdgrav3_mpi_LDADD += -littnotify -ldl
endif
if USE_CUDA
pkdgrav3_mpi_LDADD += @CUDA_LIBS@ -lcudart
endif
if USE_NVML
pkdgrav3_mpi_LDADD += @NVML_LIBS@ -lnvidia-ml
pkdgrav3_mpi_CFLAGS += @NVML_INCLUDE@
pkdgrav3_mpi_CXXFLAGS += @NVML_INCLUDE@
endif
if USE_CL
pkdgrav3_mpi_LDADD += @CL_LIBS@ -lOpenCL
pkdgrav3_mpi_CXXFLAGS += @CL_INCLUDE@
pkdgrav3_mpi_CFLAGS += @CL_INCLUDE@
endif
if USE_PYTHON
if HAVE_RDYNAMIC
pkdgrav3_mpi_LDFLAGS = -Xlinker -rdynamic 
endif
pkdgrav3_mpi_LDADD += -L$(PYTHON_LIB) -lpython$(PYTHON_VER) -ldl -lutil
pkdgrav3_mpi_CFLAGS += -I$(PYTHON_INC)
endif
if HAVE_DARWIN
pkdgrav3_mpi_SOURCES += mdl2/mac/pthread_barrier.c
pkdgrav3_mpi_CFLAGS += -Imdl2/mac/
pkdgrav3_mpi_CXXFLAGS += -Imdl2/mac/
endif
endif

tostd_SOURCES = tostd.c fio.c
tdiff_SOURCES = tdiff.c fio.c
hpb2fits_SOURCES = hpb2fits.c
lcp2tip_SOURCES = lcp2tip.c

if HAVE_CFITSIO_PATH
hpb2fits_LDADD = -L@CFITSIO_PATH@/lib -lcfitsio
hpb2fits_CFLAGS = -I @CFITSIO_PATH@/include
endif
